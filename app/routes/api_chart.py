from app import app, db
from flask_login import login_required
import json


def fill_empty_months(dataset):
    for i in range(1, 13):
        if len(dataset) >= i:
            if i != dataset[i-1]["label"]:
                dataset.insert(i-1, {'label': i, 'data': 0})
        else:
            dataset.append({'label': i, 'data': 0})
    return dataset


@app.route('/api/chart/company/get/<int:year>', methods=['GET'])
@login_required
def get_chart_company(year):
    chart_data = db.engine.execute(
         "select month 'label', total(percent) 'data'" +
         " from workload" +
         " where year = " + str(year) +
         " and user_id in (select id from user where coalesce(is_inactive, 0) = 0)" +
         " and project_id in (select id from project where coalesce(is_inactive, 0) = 0)" +
         " group by month"
         " order by month"
        ).fetchall()

    return json.dumps(fill_empty_months([dict(r) for r in chart_data]))


@app.route('/api/chart/company_project/get/<int:year>', methods=['GET'])
@login_required
def get_chart_company_project(year):
    chart_data = db.engine.execute(
         "select p.name 'label', total(w.percent) 'data'" +
         " from workload w, project p" +
         " where w.year = " + str(year) +
         " and w.project_id = p.id" +
         " and w.user_id in (select id from user where coalesce(is_inactive, 0) = 0)" +
         " and w.project_id in (select id from project where coalesce(is_inactive, 0) = 0)" +
         " group by w.project_id"
        ).fetchall()

    return json.dumps([dict(r) for r in chart_data])


@app.route('/api/chart/person/get/<int:userid>/<int:year>', methods=['GET'])
@login_required
def get_chart_person(userid, year):
    chart_data = db.engine.execute(
         "select month 'label', total(percent) 'data'" +
         " from workload" +
         " where user_id = " + str(userid) +
         " and year = " + str(year) +
         " and project_id in (select id from project where coalesce(is_inactive, 0) = 0)" +
         " group by month"
        ).fetchall()

    return json.dumps(fill_empty_months([dict(r) for r in chart_data]))


@app.route('/api/chart/project/get/<int:projectid>/<int:year>', methods=['GET'])
@login_required
def get_chart_project(projectid, year):
    chart_data = db.engine.execute(
         "select month 'label', total(percent) 'data'" +
         " from workload" +
         " where project_id = " + str(projectid) +
         " and year = " + str(year) +
         " and user_id in (select id from user where coalesce(is_inactive, 0) = 0)" +
         " group by month"
        ).fetchall()

    return json.dumps(fill_empty_months([dict(r) for r in chart_data]))
