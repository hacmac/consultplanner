from app import app, db, ma
from flask import request, jsonify
from flask_login import login_required
from app.models import Project, Workload
from app.routes.api_generic import user_is_admin, reply_ok, reply_not_ok, audit_log, Crud, Entity
from sqlalchemy import text


class ProjectSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'is_inactive', 'remark')
        ordered = True


projects_schema = ProjectSchema(many=True)


@app.route('/api/project/get_all', methods=['GET'])
@login_required
@user_is_admin
def get_all_projects():
    all_projects = Project.query.from_statement(text("SELECT * " +
                                                     "FROM project " +
                                                     "ORDER BY name COLLATE NOCASE asc")).all()

    result = projects_schema.dump(all_projects)
    return jsonify(result.data)


@app.route('/api/project/add', methods=['POST'])
@login_required
@user_is_admin
def add_project():
    try:
        project = Project(name='[new]')
        db.session.add(project)
        db.session.commit()

        audit_log(Crud.CREATE.name, Entity.PROJECT.name, project.name, None, None)
        return reply_ok()

    except Exception as ex:
        return reply_not_ok(900, 'Adding new Project FAILED! Probably there is already a project with name [new]. Rename it!')


@app.route('/api/project/update', methods=['POST'])
@login_required
@user_is_admin
def update_project():
    data = request.get_json(force=True)

    try:
        requ_id = data['id']
        requ_field = data['field']
        requ_value = data['value']

        project = Project.query.filter_by(id=requ_id).first()
        if getattr(project, requ_field) != requ_value:
            setattr(project, requ_field, requ_value)
            db.session.commit()
            audit_log(Crud.UPDATE.name, Entity.PROJECT.name, project.name, requ_field, requ_value)

        return reply_ok()

    except Exception as ex:
        return reply_not_ok(900, 'Update not done! ' + ex.__str__())


@app.route('/api/project/delete', methods=['POST'])
@login_required
@user_is_admin
def delete_project():
    data = request.get_json(force=True)

    try:
        requ_id = data['id']

        workload = Workload.query.filter_by(project_id=requ_id).first()
        if workload is None:
            project = Project.query.filter_by(id=requ_id).first()
            db.session.delete(project)
            db.session.commit()

            audit_log(Crud.DELETE.name, Entity.PROJECT.name, project.name, None, None)
            return reply_ok()
        else:
            raise ValueError('Project has assigned persons!')

    except Exception as ex:
        return reply_not_ok(900, 'Delete not done! ' + ex.__str__())

