from app import app, db, ma
from flask import request, jsonify
from flask_login import login_required
from app.models import User, Workload
from app.routes.api_generic import user_is_admin, reply_ok, reply_not_ok, audit_log, Crud, Entity
from sqlalchemy import text


class UserSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'username', 'perc', 'is_admin', 'is_inactive', 'password_hash', 'remark')
        exclude = ('password_hash', )
        ordered = True


users_schema = UserSchema(many=True)


@app.route('/api/user/get_all', methods=['GET'])
@login_required
@user_is_admin
def get_all_users():
    all_users = User.query.from_statement(text("SELECT * "
                                             + "FROM user "
                                             + "ORDER BY username COLLATE NOCASE asc")).all()

    result = users_schema.dump(all_users)
    return jsonify(result.data)


@app.route('/api/user/add', methods=['POST'])
@login_required
@user_is_admin
def add_user():
    try:
        user = User(username='[new]', is_admin=False)
        db.session.add(user)
        db.session.commit()

        audit_log(Crud.CREATE.name, Entity.USER.name, user.username, None, None)
        return reply_ok()

    except Exception as ex:
        return reply_not_ok(900, 'Adding new user FAILED! Probably there is already a user with username [new]. Rename it!')


@app.route('/api/user/update', methods=['POST'])
@login_required
@user_is_admin
def update_user():
    data = request.get_json(force=True)

    try:
        requ_id = data['id']
        requ_field = data['field']
        requ_value = data['value']

        user = User.query.filter_by(id=requ_id).first()

        if requ_field == 'password_hash':
            if requ_value is not '':
                user.set_password(requ_value)
                audit_log(Crud.UPDATE.name, Entity.USER.name, user.username, requ_field, '*hidden*')
        else:
            if getattr(user, requ_field) != requ_value:
                setattr(user, requ_field, requ_value)
                audit_log(Crud.UPDATE.name, Entity.USER.name, user.username, requ_field, requ_value)

        db.session.commit()
        return reply_ok()

    except Exception as ex:
        return reply_not_ok(900, 'Update not done! ' + ex.__str__())


@app.route('/api/user/delete', methods=['POST'])
@login_required
@user_is_admin
def delete_user():
    data = request.get_json(force=True)

    try:
        requ_id = data['id']

        workload = Workload.query.filter_by(user_id=requ_id).first()
        if workload is None:
            user = User.query.filter_by(id=requ_id).first()
            db.session.delete(user)
            db.session.commit()

            audit_log(Crud.DELETE.name, Entity.USER.name, user.username, None, None)
            return reply_ok()
        else:
            raise ValueError('User has assigned projects!')

    except Exception as ex:
        return reply_not_ok(900, 'Delete not done! ' + ex.__str__())

