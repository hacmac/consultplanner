from app import app, db, models
from flask import request
from flask_login import login_required
from app.models import Workload, User, WorkloadRemark, Project
from app.routes.api_generic import reply_ok, reply_not_ok, audit_log, Crud, Entity
from sqlalchemy import sql
import json


@app.route('/api/workload_by_person/get/<int:userid>/<int:year>', methods=['GET'])
@login_required
def get_all_workload_person(userid, year):
    all_workload_persons = db.engine.execute(
        "select id, name " +
         ", (select percent from workload w where w.project_id = p.id and w.month = 1 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '1' " +
         ", (select percent from workload w where w.project_id = p.id and w.month = 2 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '2' " +
         ", (select percent from workload w where w.project_id = p.id and w.month = 3 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '3' " +
         ", (select percent from workload w where w.project_id = p.id and w.month = 4 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '4' " +
         ", (select percent from workload w where w.project_id = p.id and w.month = 5 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '5' " +
         ", (select percent from workload w where w.project_id = p.id and w.month = 6 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '6' " +
         ", (select percent from workload w where w.project_id = p.id and w.month = 7 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '7' " +
         ", (select percent from workload w where w.project_id = p.id and w.month = 8 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '8' " +
         ", (select percent from workload w where w.project_id = p.id and w.month = 9 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '9' " +
         ", (select percent from workload w where w.project_id = p.id and w.month = 10 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '10' " +
         ", (select percent from workload w where w.project_id = p.id and w.month = 11 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '11' " +
         ", (select percent from workload w where w.project_id = p.id and w.month = 12 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '12' " +
         ", (select remark from workload_remark wr where wr.project_id = p.id and wr.user_id = " + str(userid) + ") 'remark' " +
         " from project p " +
         " where coalesce(is_inactive, 0) = 0" +
         " and exists (select * from workload ws" +   # Only show project for a user if there is a workload in this year or there was one in Dec of last year
         "   where ws.user_id = " + str(userid) +
         "   and ws.project_id = p.id" +
         "   and (year = " + str(year) + " or (year = " + str(year-1) + " and month = 12)))"
        ).fetchall()

    return json.dumps([dict(r) for r in all_workload_persons])


@app.route('/api/workload_by_project/get/<int:projectid>/<int:year>', methods=['GET'])
@login_required
def get_all_workload_project(projectid, year):
    all_workload_projects = db.engine.execute(
        "select id, name " +
         ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 1 and w.year = " + str(year) + " and w.user_id = u.id) '1' " +
         ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 2 and w.year = " + str(year) + " and w.user_id = u.id) '2' " +
         ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 3 and w.year = " + str(year) + " and w.user_id = u.id) '3' " +
         ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 4 and w.year = " + str(year) + " and w.user_id = u.id) '4' " +
         ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 5 and w.year = " + str(year) + " and w.user_id = u.id) '5' " +
         ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 6 and w.year = " + str(year) + " and w.user_id = u.id) '6' " +
         ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 7 and w.year = " + str(year) + " and w.user_id = u.id) '7' " +
         ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 8 and w.year = " + str(year) + " and w.user_id = u.id) '8' " +
         ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 9 and w.year = " + str(year) + " and w.user_id = u.id) '9' " +
         ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 10 and w.year = " + str(year) + " and w.user_id = u.id) '10' " +
         ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 11 and w.year = " + str(year) + " and w.user_id = u.id) '11' " +
         ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 12 and w.year = " + str(year) + " and w.user_id = u.id) '12' " +
         ", (select remark from workload_remark wr where wr.project_id = " + str(projectid) + " and wr.user_id = u.id) 'remark' " +
         " from user u " +
         " where coalesce(is_inactive, 0) = 0" +
         " and exists (select * from workload ws" +   # Only show user for a project if there is a workload in this year or there was one in Dec of last year
         "   where ws.user_id = u.id" +
         "   and ws.project_id = " + str(projectid) +
         "   and (year = " + str(year) + " or (year = " + str(year-1) + " and month = 12)))"
        ).fetchall()

    return json.dumps([dict(r) for r in all_workload_projects])


@app.route('/api/workload/add', methods=['POST'])
@login_required
def add_workload():
    data = request.get_json(force=True)

    try:
        requ_user_id = data['user_id']
        requ_project_id = data['project_id']
        requ_year = data['year']

        workload = Workload.query.filter_by(user_id=requ_user_id, project_id=requ_project_id, year=requ_year).first()

        if workload is None:
            workload = models.Workload(user_id=requ_user_id, project_id=requ_project_id, year=requ_year, month=1, percent=None)
            db.session.add(workload)
            db.session.commit()

            project = Project.query.filter_by(id=requ_project_id).first()
            user = User.query.filter_by(id=requ_user_id).first()
            audit_log(Crud.CREATE.name, Entity.WORKLOAD.name, project.name, user.username, None)

            return reply_ok()
        else:
            return reply_not_ok(900, 'Adding new workload_FAILED! Probably there is already a workload for user/project/year.')

    except Exception as ex:
        return reply_not_ok(900, 'Insert FAILED! ' + ex.__str__())


@app.route('/api/workload/update', methods=['POST'])
@login_required
def update_workload():
    data = request.get_json(force=True)
    try:
        requ_user_id = data['user_id']
        requ_project_id = data['project_id']
        requ_value = data['value']
        requ_year = data['year']
        requ_field = data['field']

        if requ_field == 'remark':
            workload_remark = WorkloadRemark.query.filter_by(user_id=requ_user_id, project_id=requ_project_id).first()

            if workload_remark is None:
                workload_remark = models.WorkloadRemark(user_id=requ_user_id, project_id=requ_project_id, remark=requ_value)
                db.session.add(workload_remark)
            else:
                workload_remark.remark = requ_value

            db.session.commit()

            project = Project.query.filter_by(id=requ_project_id).first()
            user = User.query.filter_by(id=requ_user_id).first()
            audit_log(Crud.UPDATE.name, Entity.WORKLREMARK.name, project.name, user.username, requ_value)

            return reply_ok()

        else:
            if requ_value == '':
                requ_value = sql.null()

            workload = Workload.query.filter_by(user_id=requ_user_id, project_id=requ_project_id, year=requ_year, month=requ_field).first()

            if workload is None:
                workload = models.Workload(user_id=requ_user_id, project_id=requ_project_id, year=requ_year, month=requ_field, percent=requ_value)
                db.session.add(workload)
            else:
                workload.percent = requ_value

            db.session.commit()

            project = Project.query.filter_by(id=requ_project_id).first()
            user = User.query.filter_by(id=requ_user_id).first()
            audit_log(Crud.UPDATE.name, Entity.WORKLOAD.name, project.name, user.username, str(requ_year) + '.' + str(requ_field) + ': ' + str(requ_value) + '%')

            return reply_ok()

    except Exception as ex:
        return reply_not_ok(900, 'Update not done! ' + ex.__str__())


@app.route('/api/workload/delete', methods=['POST'])
@login_required
def delete_workload_person():
    data = request.get_json(force=True)

    try:
        requ_user_id = data['user_id']
        requ_project_id = data['project_id']

        Workload.query.filter_by(user_id=requ_user_id, project_id=requ_project_id).delete()
        db.session.commit()

        WorkloadRemark.query.filter_by(user_id=requ_user_id, project_id=requ_project_id).delete()
        db.session.commit()

        project = Project.query.filter_by(id=requ_project_id).first()
        user = User.query.filter_by(id=requ_user_id).first()
        audit_log(Crud.DELETE.name, Entity.WORKLOAD.name, project.name, user.username, None)
        return reply_ok()

    except Exception as ex:
        return reply_not_ok(900, 'Delete FAILED! ' + ex.__str__())

