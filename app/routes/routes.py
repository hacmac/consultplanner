from app import app, db, ma, export_data
from flask import render_template, flash, redirect, url_for, request, Response
from app.forms import LoginForm, ChgpwdForm
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User, Project
from werkzeug.urls import url_parse
from datetime import datetime


class UserSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'username', 'email', 'is_admin', 'is_inactive', 'password_hash', 'remark')


users_schema = UserSchema(many=True)


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    year = request.args.get("year")
    if year is None:
        year = datetime.now().year
    if current_user.view_mode == 1:
        return render_template('workload_by_project.html', all_users=User.get_all_users(), all_projects=Project.get_all_projects(), year=int(year))
    else:
        return render_template('workload_by_person.html', all_users=User.get_all_users(), all_projects=Project.get_all_projects(), year=int(year))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        elif user.is_inactive:
            flash('User is inactive')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/useradmin', methods=['GET', 'POST'])
@login_required
def useradmin():
    return render_template('useradmin.html')


@app.route('/projectadmin', methods=['GET', 'POST'])
@login_required
def projectadmin():
    return render_template('projectadmin.html')


@app.route('/chgpwd', methods=['GET', 'POST'])
@login_required
def chgpwd():
    form = ChgpwdForm()
    try:
        if form.validate_on_submit():
            current_user.set_password(form.password.data)
            db.session.commit()
            flash('Password changed')
            return redirect(url_for('index'))
        return render_template('chgpwd.html', form=form)
    except Exception as ex:
        flash('Update not done! ' + ex.__str__())
        return render_template('chgpwd.html', form=form)


@app.route('/toggle_grouping', methods=['GET', 'POST'])
@login_required
def toggle_grouping():
    if current_user.view_mode == 1:
        current_user.view_mode = 2
    else:
        current_user.view_mode = 1
    db.session.commit()
    return redirect(url_for('index'))


@app.route('/chart', methods=['GET', 'POST'])
@login_required
def chart():
    year = request.args.get("year")
    if year is None:
        year = datetime.now().year
    return render_template('chart.html', all_users=User.get_all_users(), all_projects=Project.get_all_projects(), year=int(year))


@app.route('/auditlog', methods=['GET', 'POST'])
@login_required
def auditlog():
    return render_template('auditlog.html')


@app.route('/export', methods=['GET', 'POST'])
@login_required
def export():
    return Response(export_data.generate(), mimetype='text/csv')
