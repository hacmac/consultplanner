from flask_login import current_user, login_required
from flask import jsonify
from json import dumps
from functools import wraps
from app.models import Audit
from app import app, db, ma
from enum import Enum
from sqlalchemy import text


# CONSTANTS for log
class Crud(Enum):
    CREATE = 'CREATE'
    READ   = 'READ'
    UPDATE = 'UPDATE'
    DELETE = 'DELETE'

class Entity(Enum):
    USER        = 'USER'
    PROJECT     = 'PROJECT'
    WORKLOAD    = 'WORKLOAD'
    WORKLREMARK = 'ŴORKLOAD_REMARK'


class AuditSchema(ma.Schema):
    class Meta:
        fields = ('id', 'chg_type', 'chg_table', 'chg_object', 'chg_field', 'chg_new_value', 'timestamp', 'username')
        dateformat = "%Y.%m.%d %H:%M:%S"
        ordered = True


audit_schema = AuditSchema(many=True)


# user_is_admin decorator
def user_is_admin(func):
    @wraps(func)
    def check_admin(*args, **kwargs):
        if current_user.is_admin:
            return func(*args, **kwargs)
        else:
            return "permission error", "900 User is not Admin"
    return check_admin


def reply_ok():
    return dumps({'status': 'OK'})


def reply_not_ok(code, err_text):
    code = str(code)
    return "status error", code + ' ' + err_text


def audit_log(chg_type, chg_table, chg_object, chg_field, chg_new_value):
    log = Audit(username=current_user.username)
    log.chg_type = chg_type
    log.chg_table = chg_table
    log.chg_object = chg_object
    log.chg_field = chg_field
    log.chg_new_value = chg_new_value

    db.session.add(log)
    db.session.commit()


@app.route('/api/audit/get_all', methods=['GET'])
@login_required
def get_all_auditlogs():
    all_logs = Audit.query.from_statement(text("SELECT id "
                                             + "FROM audit "
                                             + "ORDER BY timestamp desc "
                                             + "LIMIT 1000")).all()
    result = audit_schema.dump(all_logs)
    return jsonify(result.data)
