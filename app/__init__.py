from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
import logging
from logging.handlers import RotatingFileHandler
import os
from flask_marshmallow import Marshmallow


app = Flask(__name__)
app.config.from_object(Config)

db = SQLAlchemy(app)
migrate = Migrate(app, db)

login = LoginManager(app)
login.login_view = 'login'

ma = Marshmallow(app)

bootstrap = Bootstrap(app)

from app import models, errors
from app.routes import routes, api_user, api_project, api_workload, api_chart


# Add initial user (admin)
# @todo: do only if application is started normally, not for db migrate/upgrade
from app.models import User

try:
    if models.User.query.count() == 0:
        user = models.User(username='admin', name='admin', is_admin=True)
        user.set_password('admin')
        db.session.add(user)
        db.session.commit()
        print('Initial admin user created.')
except Exception as ex:
    print('It was not possible to create the initial admin user: ', ex)


if not app.debug:
    if not os.path.exists('logs'):
        os.mkdir('logs')
    file_handler = RotatingFileHandler('logs/planner.log', maxBytes=10240, backupCount=10)
    file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.setLevel(logging.INFO)
    app.logger.info('ConsultPlanner Startup')

