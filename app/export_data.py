from app import db
from app.models import User, Project
from datetime import datetime
from csv import writer
import io


def generate():
    f = io.StringIO()
    csv = writer(f)
    f.write('ConsultPlanner Export per ' + str(datetime.now()))
    f.write(str(chr(10) + chr(13)) * 2)

    for i in range(0, 2):
        year = datetime.now().year + i

        f.write('By Person ' + str(year))
        f.write(str(chr(10) + chr(13)) * 2)

        for u in User.get_all_users():
            userid = u.id

            all_workload_persons = db.engine.execute(
            "select id, name " +
             ", (select percent from workload w where w.project_id = p.id and w.month = 1 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '1' " +
             ", (select percent from workload w where w.project_id = p.id and w.month = 2 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '2' " +
             ", (select percent from workload w where w.project_id = p.id and w.month = 3 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '3' " +
             ", (select percent from workload w where w.project_id = p.id and w.month = 4 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '4' " +
             ", (select percent from workload w where w.project_id = p.id and w.month = 5 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '5' " +
             ", (select percent from workload w where w.project_id = p.id and w.month = 6 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '6' " +
             ", (select percent from workload w where w.project_id = p.id and w.month = 7 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '7' " +
             ", (select percent from workload w where w.project_id = p.id and w.month = 8 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '8' " +
             ", (select percent from workload w where w.project_id = p.id and w.month = 9 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '9' " +
             ", (select percent from workload w where w.project_id = p.id and w.month = 10 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '10' " +
             ", (select percent from workload w where w.project_id = p.id and w.month = 11 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '11' " +
             ", (select percent from workload w where w.project_id = p.id and w.month = 12 and w.year = " + str(year) + " and w.user_id = " + str(userid) + ") '12' " +
             ", (select remark from workload_remark wr where wr.project_id = p.id and wr.user_id = " + str(userid) + ") 'remark' " +
             " from project p " +
             " where coalesce(is_inactive, 0) = 0" +
             " and exists (select * from workload ws" +   # Only show project for a user if there is a workload in this year or there was one in Dec of last year
             "   where ws.user_id = " + str(userid) +
             "   and ws.project_id = p.id" +
             "   and (year = " + str(year) + " or (year = " + str(year-1) + " and month = 12)))"
            ).fetchall()

            if len(all_workload_persons) > 0:
                f.write(u.name)
                f.write(chr(10) + chr(13))
                csv.writerow(all_workload_persons[0].keys())
                csv.writerows(all_workload_persons)
                f.write(chr(10) + chr(13))

        f.write(chr(10) + chr(13))

    for i in range(0, 2):
        year = datetime.now().year + i

        f.write('By Project ' + str(year))
        f.write(str(chr(10) + chr(13)) * 2)

        for p in Project.get_all_projects():
            projectid = p.id

            all_workload_projects = db.engine.execute(
               "select id, name " +
                ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 1 and w.year = " + str(year) + " and w.user_id = u.id) '1' " +
                ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 2 and w.year = " + str(year) + " and w.user_id = u.id) '2' " +
                ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 3 and w.year = " + str(year) + " and w.user_id = u.id) '3' " +
                ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 4 and w.year = " + str(year) + " and w.user_id = u.id) '4' " +
                ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 5 and w.year = " + str(year) + " and w.user_id = u.id) '5' " +
                ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 6 and w.year = " + str(year) + " and w.user_id = u.id) '6' " +
                ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 7 and w.year = " + str(year) + " and w.user_id = u.id) '7' " +
                ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 8 and w.year = " + str(year) + " and w.user_id = u.id) '8' " +
                ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 9 and w.year = " + str(year) + " and w.user_id = u.id) '9' " +
                ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 10 and w.year = " + str(year) + " and w.user_id = u.id) '10' " +
                ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 11 and w.year = " + str(year) + " and w.user_id = u.id) '11' " +
                ", (select percent from workload w where w.project_id = " + str(projectid) + " and w.month = 12 and w.year = " + str(year) + " and w.user_id = u.id) '12' " +
                ", (select remark from workload_remark wr where wr.project_id = " + str(projectid) + " and wr.user_id = u.id) 'remark' " +
                " from user u " +
                " where coalesce(is_inactive, 0) = 0" +
                " and exists (select * from workload ws" +   # Only show user for a project if there is a workload in this year or there was one in Dec of last year
                "   where ws.user_id = u.id" +
                "   and ws.project_id = " + str(projectid) +
                "   and (year = " + str(year) + " or (year = " + str(year-1) + " and month = 12)))"
               ).fetchall()

            if len(all_workload_projects) > 0:
                f.write(p.name)
                f.write(chr(10) + chr(13))
                csv.writerow(all_workload_projects[0].keys())
                csv.writerows(all_workload_projects)
                f.write(chr(10) + chr(13))

        f.write(chr(10) + chr(13))

    f.close
    return f.getvalue()
