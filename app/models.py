from datetime import datetime
from app import db, login
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120))
    password_hash = db.Column(db.String(128))
    is_inactive = db.Column(db.Boolean)
    is_admin = db.Column(db.Boolean)
    remark = db.Column(db.String(128))
    view_mode = db.Column(db.Integer) # 1 = by Project; 2 = by Person
    perc = db.Column(db.Integer)  # 100% = Full Time Employee

    def set_password(self, password):
        if len(password) > 4:
            self.password_hash = generate_password_hash(password)
        else:
            raise ValueError('Password minimum length 5 characters')

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @staticmethod
    def get_all_users():
        return User.query.filter(User.is_inactive.isnot(True)).order_by(User.name).all()


class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(400))
    remark = db.Column(db.String(128))
    is_inactive = db.Column(db.Boolean)

    @staticmethod
    def get_all_projects():
        return Project.query.filter(Project.is_inactive.isnot(True)).order_by(Project.name).all()


class Workload(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), index=True)
    project_id = db.Column(db.Integer, db.ForeignKey('project.id'), index=True)
    percent = db.Column(db.Float)
    month = db.Column(db.Integer, index=True)
    year = db.Column(db.Integer, index=True)
    last_change_user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


class WorkloadRemark(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), index=True)
    project_id = db.Column(db.Integer, db.ForeignKey('project.id'), index=True)
    last_change_user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    remark = db.Column(db.String(128))


class Audit(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    chg_type = db.Column(db.String(20))
    chg_table = db.Column(db.String(100))
    chg_object = db.Column(db.String(100))
    chg_field = db.Column(db.String(100))
    chg_new_value = db.Column(db.String(400))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    username = db.Column(db.String(64))


@login.user_loader
def load_user(id):
    return User.query.get(int(id))



