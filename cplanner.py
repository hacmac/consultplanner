from app import app, db
from app.models import User, Project, Workload, Audit

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Project': Project, 'Workload': Workload, 'Audit': Audit}